#include <iostream>
#include "List.h"

using namespace std;

int main()
{
	int i = 0;

	List L1, L2; //Declare two list objects, L1 and L2


	cout << "Welcome to my List ADT client"<<endl<<endl;

	 //Do some stuff with L1, L2, ... (Eg. cout<<L1.size(); )
	 // ...
	 
	cout << L1.size() << endl;
	 
	for (i = 0; i < 10; i++)
	{
		L1.insert((i + 5), (i + 1));
	}
	
	cout << L1.getNode(5) << endl;
	L1.insert(21,3);
	cout << L1.size() << "\n" << endl;
	
	for (int i = 1; i <= L1.size(); i++)
	{
		cout << "L1 - " << L1.getNode(i) << endl;
	}
	
	cout << "Copy L1 into L2 then erase data from L1" << endl;
	
	for (int i = 1; i < L1.size(); i++)
	{
		L2.insert(L1.getNode(i), i);
	}
	
	L1.clear();
	cout << "L1.size() - " << L1.size() << endl;
	
	for (int i = 1; i <= L2.size(); i++)
	{
		cout << "L2 - " << L2.getNode(i) << endl;
	}

}
